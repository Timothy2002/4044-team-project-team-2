DROP DATABASE LTUbike-db;
CREATE DATABASE LTUbike-db;
USE LTUbike-db;
DROP TABLE IF EXISTS User, Pic, User-payment ;


CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;


INSERT INTO User (email, first-name, last-name, password, is_admin) VALUES
  ("admin@trinitybike.com", "fergus", "phorscin", "myphorscinis1123", "1"),
  ("steveharvey@gmail.com", "barry", "presscot", "ilikbutts69", 0),
  ("sorrelharriet@leedstrinity.co.uk", "sorrel", "Harriet", "inlovingmemory", 0),
  ("Johndow@example.com", "John", "Dow", "password1", 0);

CREATE TABLE User-payment (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO User-payment (email ) VALUES


CREATE TABLE Pic (
	id INT AUTO_INCREMENT,
	url VARCHAR(200) NOT NULL,
	room_id INT,
	PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES Room (id)
) ENGINE=InnoDB;

INSERT INTO Pic (room_id, url) VALUES
	(1, " "),
	(2, " "),
	(3, " "),
    (4, " "),
    (5, " "),
	(6, " "),
    (7, " "),
    (8, " ");