# Bet888 App 
## Implemented with Flask and MySQL on Docker

This implementation of bet8888 app is built with the Flask microframework. The database backend is MySQL. It runs inside Docker.

+ All of the application code written in python is in the 'app/' directory
+ The SQL statements required to setup the database is in 'db/setup.sql'

### Install

To build and run the application, make sure Docker is running first.

+ From Windows Powershell in this file location run:

		docker-compose build
		docker-compose up -d

The above command will build the application according to the 'recipe' dictated in 'docker-compose.yaml'.

+ Visit the app in the browser: [localhost:5000](http://localhost:5000) 
to view the web-page

To stop the app do:

		docker-compose stop


### Making changes to the application source code

You can edit the source code but you will need to rebuild the application after editing. This is because the running version of the application is actually a copy of the source code which is running in the docker container meaning that editing the files here won't have any effect to the view of the webpage.

Simply repeat the installation step above each time you make changes to the source code.

Then to see the changes made simpley type from Windows Powershell in this file location:

		ctrl+F5

This would refresh the page to allow you to see the cahgnes made to the coding

### Making changes to the database

You can edit the SQL statements in 'db/setup.sql' if you want to change the database schema or add different data.

However you need to make sure the database is recreated for the changes to have any effect. 

The simplest way to do this is to destroy the container running the database and rebuild the app.

		docker rm bet8888-mysql
		docker-compose build
		docker-compose up -d