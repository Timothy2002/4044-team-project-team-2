# edit and use this config if you are running app in docker
class DockerConfig(object):
    SECRET_KEY = "my super secret key".encode('utf8')
    MYSQL_DATABASE_HOST = 'ltudigz-mysql'
    MYSQL_DATABASE_USER = 'user'
    MYSQL_DATABASE_PASSWORD = 'password'
    MYSQL_DATABASE_DB = 'digz_db'
    DEBUG = True

# edit and use this config if you are running app locally
class Config(object):
    SECRET_KEY = "my super secret key".encode('utf8')
    MYSQL_DATABASE_HOST = 'localhost'
    MYSQL_DATABASE_USER = ''
    MYSQL_DATABASE_PASSWORD = ''
    MYSQL_DATABASE_DB = 'digz_db'
    DEBUG = True
