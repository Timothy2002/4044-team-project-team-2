from flask import Flask, render_template

app = Flask(__name__)

digz_data = [
        {
            "agent_id": 220,
            "type": "house share",
            "price": {"billing_period": "pcm", "amount": "350"},
            "locality": "Burley",
            "bed": "double",
            "ensuite": False,
            "pics": ["https://i.ebayimg.com/00/s/NzY4WDEwMjQ=/z/q8QAAOSwt0dcUH-N/$_86.JPG", "https://i.ebayimg.com/00/s/NzY4WDEwMjQ=/z/NnkAAOSwm~1cUH-c/$_86.JPG"]
        },
        {
            "agent_id": 1,
            "type": "halls of residence",
            "price": {"billing_period": "pw", "amount": "139"},
            "locality": "Horsforth",
            "bed": "single",
            "ensuite": True,
            "pics": ["https://farm5.staticflickr.com/4135/4900473063_d095bba159_b.jpg"]
        }
]

@app.route('/')
def home():
    return render_template('home.html', page='Home', data=digz_data)

if __name__ == '__main__':
  app.run(debug=True)
